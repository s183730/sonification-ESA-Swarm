# Import packages
import numpy as np
import scipy.signal as signal # DSP Toolbox
import scipy.io.wavfile as wav # WAV-file I/O
import pandas as pd # CSV-file handling
import resampy # anti-aliasing resampling
import pyrubberband as pyrb # time-streching and frequency shifting
import argparse # for argument parsing
from hapiclient import hapi, hapitime2datetime
import datetime as dt

## -- Handle input arguments -- ##
all_args = argparse.ArgumentParser(description="Generate a direct audification of the data from the ESA Swarm satellites. As a default, the script will collect the needed data from the VirES server, by setting a start date with the argument \"-t0\" and an end date with the argument \"-t1\". If the '--csvFile' option is selected, then a path to a CSV file containing the data must be given with the '-p' argument.")
all_args.add_argument("-t0", "--StartDate", default="2022-01-16 00:00:00", help="Start date of data retrieval. Format should be \"year-month-date hours:minutes:seconds\" e.g \"2022-01-16 12:54:00\".", type=str)
all_args.add_argument("-t1", "--EndDate", default="2022-01-23 23:59:59", help="End date of data retrieval. Format should be \"year-month-date hours:minutes:seconds\" e.g \"2022-01-16 12:54:00\".", type=str)
all_args.add_argument("--Parameter", default="F", help="Select the parameter that will be sonified. The parameter needs to be a scaler and NOT a vector. A list of parameters can be found on: https://vires.services/hapi/. The default parameter is the magnitude \"F\".")
all_args.add_argument("--Length", default=10, help="Selects final length in seconds of the sonification: Default: 10.0 seconds", type=float)
all_args.add_argument("--PitchShift", default=0, help="Select the amount of pitch shifting in semitones: Default: 0.0 semitones", type=float)
all_args.add_argument("--WAVname", default="sonification.wav", help="Speficy the name/path of the generated WAV file. Default: \"sonification.wav\" ", type=str)
all_args.add_argument("--Satellite", default="A", help="Select if the Alpha or Bravo satellite should be used for sonification. The argument \"A\" will select the Alpha satellite and the argument \"B\" will select the Bravo satellite. The default is \"A\".")
all_args.add_argument("--dualSatellite", action=argparse.BooleanOptionalAction, help="Enables stereo sonification with the Alpha satellite in the left channel and the Bravo satellite in the right channel.")
all_args.add_argument("--csvFile", action=argparse.BooleanOptionalAction, help="If this option is selected, a CSV file given by the '-p' argument will be selected for the sonification.")
all_args.add_argument("-p", "--Path", default="./datafile.csv", help="The path to the CSV file downloaded from the Swarm VirES client. This should be given i the '--csvFile' option is selected.")
args = vars(all_args.parse_args())

## -- FUNCTIONS -- ##

def loadData(path, satellite="A", param="F"):
    '''Get the data from the VirES CSV file. 'dataType' can be used to select what type of data is need.'''
    data = pd.read_csv(path)
    print("Content of entire CSV file:\n")
    print(data)

    # Extract the correct column of data
    satdata = data.loc[data["Spacecraft"] == satellite]
    x = satdata.get(param)
    x.fillna(0)
    print(f'Extracted data from satellite {satellite}:\n')
    print(x)
    return x, satdata


## Function inspired by Ashley Smith: https://github.com/Swarm-DISC/fun-with-swarm/blob/main/notebooks/sounds.ipynb
def fetch_data(t0, t1, satellite="A", param="F") -> pd.DataFrame:
    """Fetch data from VirES HAPI
    
    This needs to be done in chunks due to the limit, x_maxTimeSelection
    """
    dataset = "SW_OPER_MAG" + satellite + "_LR_1B"
    # Generate time chunks
    times = pd.date_range(start=t0, end=t1, freq="D").to_pydatetime()
    start_times = times[:-1]
    end_times = times[1:]
    len_tot = len(start_times)
    counter = 0
    # Build dataframe in chunks of _df
    df = pd.DataFrame()
    for start_time, end_time in zip(start_times, end_times):
        # Fetch data
        data, meta = hapi(
            "https://vires.services/hapi/",
            dataset,
            param,
            start_time.isoformat(),
            end_time.isoformat(),
        )
        counter = counter + 1
        pcnt = 100 * counter/len_tot
        print('%3.2f percent of the data downloaded for satellite %s' %(pcnt, satellite), end='\r')
        # Convert to dataframe
        #  To fix: this will not work with vector, e.g. B_NEC
        _df = pd.DataFrame(columns=data.dtype.names, data=data)
        _df = _df.set_index("Timestamp")
        _df.index = hapitime2datetime(_df.index.values.astype(str))
        _df.index = _df.index.tz_convert("UTC").tz_convert(None)
        df = pd.concat([df, _df])
    print("\n")
    return df.get(param)


def getSamplingRate(data):
    '''Get sample rate from timestamp. Input is a Pandas DataFrame'''
    time = data[0:2].get("Timestamp")

    timeNative = []

    for index in time:
        tmp = pd.to_datetime(index, format='%Y-%m-%dT%H:%M:%S') # convert to a Timestamp object
        timeNative.append(tmp.timestamp())                      # convert to POSIX timestamp

    fs = (timeNative[1] - timeNative[0])**-1

    print(f'Sampling frequency of Swarm data: {fs}Hz')
    return fs


def normalizeSignal(magnitude) -> np.ndarray:
    '''Convert magnitude data into normalized +1/-1 floating point range'''
    magnitude[np.isnan(magnitude)] = 0
    magMax = magnitude.max()
    magMin = magnitude.min()

    # remove DC/average
    magDC = np.nanmean(magnitude)
    magOffsetRemoved = magnitude - magDC

    # calculate peak-to-peak and normalize
    ptp = (magMax-magMin)
    magScaled = magOffsetRemoved / ptp
    return magScaled


def highpassFilter(x, fs=1, N=8, fc=0.0001):
    '''Create a Butterworth highpass filter to remove the DC offset'''
    sos = signal.butter(N, fc, 'highpass', fs=fs, output='sos')
    return signal.sosfilt(sos, x)


def resampling(x, resampling_factor=7, fs=1, fs_new=44100):
    '''Resampling of the original signal. The resampling factor is set empirically.'''
    y = resampy.resample(x, fs_new, int(fs_new/(resampling_factor*fs)))
    T_length = len(y) / fs_new
    return y


def smoothingWindow(x, fs=44100, t_fade=0.1):
    ''' Create smoothing window to avoid clicks and pops in the start and end of the signal'''
    window = np.ones(len(x))
    L = int(t_fade * fs)
    fade = np.linspace(0,1,L)    

    for i in range(L):
        window[i] *= fade[i]
        window[len(window)-1-i] *= fade[i]

    y = x * window
    return y


def timeStretching(x, T_length_TS, fs=44100):
    '''Time stretching. T_length_TS is in seconds'''
    T_length = len(x) / fs
    TS_ratio = T_length / T_length_TS
    y = pyrb.time_stretch(x, fs, TS_ratio)
    return y


def pitchShifting(x, semitones=0, fs=44100):
    '''Pitch shifting'''
    return pyrb.pitch_shift(x, fs, semitones)


def saveWav(x, filename, fs=44100):
    save = True
    if save:
        wav_format = np.int16
        wav_amp_scaling = np.iinfo(wav_format).max
        data = wav_amp_scaling*x
        wav.write(filename, fs, data.astype(wav_format))
        print(f'Wav-file saved as \"{filename}\" {fs}Hz {np.iinfo(wav_format).bits} bits\n')
    else:
        print(f'Wav-file not saved\n')


def sonifyDSPchain(x, fs=1):
    '''
    - Normalize magnitude data
    - Create highpass filter
    - Resample: assumed low resolution data (1Hz sampling rate)
    - Apply smoothing window
    - Apply time stretching 
    - Apply pitch shifting'''
    x = x.values
    x = normalizeSignal(x)
    x = highpassFilter(x, fs=fs)
    x = resampling(x)
    x = smoothingWindow(x)
    x = timeStretching(x, T_length_TS=args["Length"])
    x = pitchShifting(x, semitones=args["PitchShift"])
    return x

## --------------- ##

### MAIN SCRIPT ###

if not args["csvFile"]:
    t0 = dt.datetime.strptime(args["StartDate"], "%Y-%m-%d %H:%M:%S")
    t1 = dt.datetime.strptime(args["EndDate"], "%Y-%m-%d %H:%M:%S")
    print('\nLoading data...\n')
    if args["dualSatellite"]:
        xA = fetch_data(t0, t1, satellite="A", param=args["Parameter"])
        xB = fetch_data(t0, t1, satellite="B", param=args["Parameter"])
    else:
        x = fetch_data(t0, t1, satellite=args["Satellite"], param=args["Parameter"])
    print('-- DATA LOADED --\n')
    if args["dualSatellite"]:
        xA = sonifyDSPchain(xA) 
        xB = sonifyDSPchain(xB)
    else:
        x = sonifyDSPchain(x)

    ### SAVE WAV FILE ###
    if args["dualSatellite"]:
        saveWav(np.stack((xA, xB), axis=-1), filename=args["WAVname"])
    else:
        saveWav(x, filename=args["WAVname"])

else:
    x, data = loadData(path=args["Path"], satellite=args["Satellite"], param=args["Parameter"])
    print('-- DATA LOADED --\n')
    fs = getSamplingRate(data)
    x = sonifyDSPchain(x, fs=fs)
    saveWav(x, filename=args["WAVname"])

print(f'Final length of sonification after resampling and timestretching: {args["Length"]:.2f}s')
print(f'Amount of pitch shifting applied to the data: {args["PitchShift"]:.2f} semitones\n')


